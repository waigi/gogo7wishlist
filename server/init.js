Meteor.startup(function() {
  UploadServer.init({
    tmpDir: process.env.PWD + '/public/img/products/tmp',
    uploadDir: process.env.PWD + '.public/img/products/'
  });
});
