// 
Carts = new Mongo.Collection('carts');

Carts.cartWithUsername = function(username) {
  return Carts.find({username: username}).fetch();
}

Carts.cartWithUsernameAndProductId = function(username, productId) {
  return Carts.findOne({username: username, productId: productId});
}

Meteor.methods({
  removeCartWithUsernameAndProductId: function(username, productId) {
  	Carts.remove({username: username, productId: productId});
  },
  
  removeCart: function(cartItemId) {
  	Carts.remove(cartItemId);
  },
  
  removeCartWithUsername: function(username) {
  	Carts.remove({username: username});
  }
  
});