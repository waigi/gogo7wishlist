Products = new Mongo.Collection('products');

Products.hotProducts = function() {
  return Products.find({hotFlag: true}, {sort: {displayOrder: -1, date: -1}}).fetch();
}

if (Meteor.isClient) {
	Template.products.helpers({
		products: function () {
			return Products.find({ourPrice: {$not: ''}}, {sort: {displayOrder: -1, createdAt: -1}});
		},
		productsInStock: function() {
			return Products.find({stockCount: {$gt: 0}});	
		}
	});

  Template.productsLuxury.helpers({
    productsLuxury: function () {
      return Products.find({$and:[{luxryFlag :true}, {ourPrice: {$ne : ""}}]}, {sort: {displayOrder: -1, createdAt: -1}, limit: productsHandle.limit()});
    },
    productsLuxuryReady: function() {
      return ! productsHandle.loading(); 
    },
    allProductsLuxuryLoaded: function() {
      return ! productsHandle.loading() && Products.find({$and:[{luxryFlag :true}, {ourPrice: {$ne : ""}}]}, {sort: {displayOrder: -1, createdAt: -1}}).count() < productsHandle.loaded(); 
    }
  });

  Template.productsBaby.helpers({
    productsBaby: function () {
      return Products.find({$and:[{babyFlag :true}, {ourPrice: {$ne : ""}}]}, {sort: {displayOrder: -1, createdAt: -1}, limit: productsHandle.limit()});
    },
    productsBabyReady: function() {
      return ! productsHandle.loading(); 
    },
    allProductsBabyLoaded: function() {
      return ! productsHandle.loading() && Products.find({$and:[{babyFlag :true}, {ourPrice: {$ne : ""}}]}, {sort: {displayOrder: -1, createdAt: -1}}).count() < productsHandle.loaded(); 
    }
  });

  Template.productsHealthfood.helpers({
    productsHealthfood: function () {
      return Products.find({$and:[{healthfoodFlag :true}, {ourPrice: {$ne : ""}}]}, {sort: {displayOrder: -1, createdAt: -1}, limit: productsHandle.limit()});
    },
    productsHealthfoodReady: function() {
      return ! productsHandle.loading(); 
    },
    allProductsHealthfoodLoaded: function() {
      return ! productsHandle.loading() && Products.find({$and:[{healthfoodFlag :true}, {ourPrice: {$ne : ""}}]}, {sort: {displayOrder: -1, createdAt: -1}}).count() < productsHandle.loaded(); 
    }
  });

  Template.productsCosmetics.helpers({
    productsCosmetics: function () {
      return Products.find({$and:[{cosmeticsFlag :true}, {ourPrice: {$ne : ""}}]}, {sort: {displayOrder: -1, createdAt: -1}, limit: productsHandle.limit()});
    },
    productsCosmeticsReady: function() {
      return ! productsHandle.loading(); 
    },
    allProductsCosmeticsLoaded: function() {
      return ! productsHandle.loading() && Products.find({$and:[{cosmeticsFlag :true}, {ourPrice: {$ne : ""}}]}, {sort: {displayOrder: -1, createdAt: -1}}).count() < productsHandle.loaded(); 
    }
  });

  Template.products.events({
    'click .luxuryProducts': function() {
      //alert('luxury');
      Session.set('productsCategory', 'luxury');
    },
    'click .babyProducts': function() {
      //alert('babyProducts');
      Session.set('productsCategory', 'baby');
    },
    'click .healthfoodProducts': function() {
      //alert('healthfoodProducts');
      Session.set('productsCategory', 'healthfood');
    },
    'click .cosmeticsProducts': function() {
      //alert('cosmeticsProducts');
      Session.set('productsCategory', 'cosmetics');
    },

  });

  Template.productsLuxury.events({
    'click .more-link': function() {
      event.preventDefault();
      productsHandle.loadNextPage();
    }
  });

  Template.productsBaby.events({
    'click .more-link': function() {
      event.preventDefault();
      productsHandle.loadNextPage();
    }
  });

  Template.productsHealthfood.events({
    'click .more-link': function() {
      event.preventDefault();
      productsHandle.loadNextPage();
    }
  });

  Template.productsCosmetics.events({
    'click .more-link': function() {
      event.preventDefault();
      productsHandle.loadNextPage();
    }
  });

}


Meteor.methods({
	deleteProduct: function(productId) {
		var product = Products.findOne(productId);
		
		Products.remove(productId);
	}
});