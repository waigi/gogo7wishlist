Brands = new Mongo.Collection('brands');

Brands.brandWithName = function(brandName) {
  return Brands.findOne({name: brandName});
}

// insert initial data
if (Meteor.isServer && Brands.find().count() === 0) {
  Meteor.startup(function() {
    Brands.insert({
      name: 'ChildLife'
    });
    Brands.insert({
      name: 'Keihl\'s'
    });
    Brands.insert({
      name: 'Earth Mama'
    });
    Brands.insert({
      name: 'Angle Baby'
    });
    Brands.insert({
      name: 'Rush'
    });
  });
}