var feedSubscription;

// Handle for launch screen possibly dismissed from app-body.js
dataReadyHold = null;

// Global subscriptions
if (Meteor.isClient) {
  Meteor.subscribe('news');
  Meteor.subscribe('bookmarkCounts');
  feedSubscription = Meteor.subscribe('feed');
  //Meteor.subscribe('products');
  productsHandle = Meteor.subscribeWithPagination('products', 10);
  productsHandle = Meteor.subscribeWithPagination('luxuryProducts', 10);
  productsHandle = Meteor.subscribeWithPagination('babyProducts', 10);
  productsHandle = Meteor.subscribeWithPagination('healthfoodProducts', 10);
  productsHandle = Meteor.subscribeWithPagination('cosmeticsProducts', 10);
  Meteor.subscribe('carts');
  Meteor.subscribe('orders');
  //Meteor.subscribe('userData');
}

Router.configure({
  layoutTemplate: 'appBody',
  //loadingTemplate: 'loading',
  //waitOn: function() {},
  notFoundTemplate: 'notFound'
});

if (Meteor.isClient) {
  // Keep showing the launch screen on mobile devices until we have loaded
  // the app's data
  dataReadyHold = LaunchScreen.hold();
	
	/*
	// user account
	Accounts.ui.config({
		passwordSignupFields: "USERNAME_AND_EMAIL"	
	});
	*/
}

HomeController = RouteController.extend({
  onBeforeAction: function() {
    Meteor.subscribe('latestActivity', function() {
      dataReadyHold.release();
    });
    this.next();
  }
});

FeedController = RouteController.extend({
  onBeforeAction: function() {
    this.feedSubscription = feedSubscription;
    this.next();
  }
});

RecipesController = RouteController.extend({
  data: function() {
    return _.values(RecipesData);
  }
});

BookmarksController = RouteController.extend({
  onBeforeAction: function() {
    if (Meteor.user())
      Meteor.subscribe('bookmarks');
    else
      Overlay.open('authOverlay');
      this.next();
  },
  data: function() {
    if (Meteor.user())
      return _.values(_.pick(RecipesData, Meteor.user().bookmarkedRecipeNames));
  }
});

RecipeController = RouteController.extend({
  onBeforeAction: function() {
    Meteor.subscribe('recipe', this.params.name);
    this.next();
  },
  data: function() {
    return RecipesData[this.params.name];
  }
});

AdminController = RouteController.extend({
  onBeforeAction: function() {
    Meteor.subscribe('news');
    this.next();
  }
});

Router.map(function() {
  this.route('home', {
  	path: '/'
  });
  this.route('feed');
  this.route('recipes');
  this.route('bookmarks');
  this.route('about');
  this.route('recipe', {path: '/recipes/:name'});
  this.route('admin', { layoutTemplate: null });
  
  // wish list
  this.route('products');
  this.route('productsLuxury');
  this.route('productsBaby');
  this.route('productsHealthfood');
  this.route('productsCosmetics');
  /*
  this.route('productsLuxury', {
    data: function() { return Products.find({$and:[{luxryFlag :true}, {ourPrice: {$not: {$ne : ""}}}]}, {sort: {displayOrder: -1, createdAt: -1}}); }
  });
  this.route('productsBaby', {
    data: function() { return Products.find({$and:[{babyFlag :true}, {ourPrice: {$not: {$ne : ""}}}]}, {sort: {displayOrder: -1, createdAt: -1}}); }
  });
  this.route('productsHealthfood', {
    data: function() { return Products.find({$and:[{healthfoodFlag :true}, {ourPrice: {$not: {$ne : ""}}}]}, {sort: {displayOrder: -1, createdAt: -1}}); }
  });
  this.route('productsCosmetics', {
    data: function() { return Products.find({$and:[{cosmeticsFlag :true}, {ourPrice: {$not: {$ne : ""}}}]}, {sort: {displayOrder: -1, createdAt: -1}}); }
  });
  */
  this.route('product', {
  	path: '/products/:_id', 
  	data: function() { return Products.findOne(this.params._id); }
  });
  //this.route('product', {path: '/products/:_id'});
  this.route('cart');
  this.route('account');
  // admin
  this.route('productSubmit', {
    path: '/productSubmit',
    layoutTemplate: 'productSubmit',
  });
  this.route('productEdit', {
    path: '/products/:_id/edit',
    layoutTemplate: 'productEdit',
    data: function() { return Products.findOne(this.params._id); }
  });
  this.route('newsSubmit', {
    layoutTemplate: 'newsSubmit'
  });
  
});

Router.onBeforeAction('dataNotFound', {only: 'recipe'});

//Router.onBeforeAction('loading');