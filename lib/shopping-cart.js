ShoppingCart = function ShoppingCart() {
  this.Items = new Array();
  this.grandTotal = 0.00;
}

ShoppingCart.prototype.addItem = function(Item){
  this.Items.push(Item);
  this.Items.sort();
  this.calculateTotal();
}