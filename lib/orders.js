//
Orders = new Mongo.Collection('orders');

Orders.orderWithUsername = function(username) {
  return Orders.find({username: username});
}

Orders.incompletedOrderWithUsername = function(username) {
  return Orders.find({username: username, completed: false});
}