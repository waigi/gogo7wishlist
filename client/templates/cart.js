// 购物车页面
Template.cart.helpers({
  itemsInCart: function() {
  	return Carts.cartWithUsername(Meteor.user().username);
  },
  
  hasItemsInCart: function() {
  	return Carts.cartWithUsername(Meteor.user().username).length > 0;
  },
  
  productWithId: function(productId) {
  	return Products.findOne({_id: productId});
  },
  
  grandTotal: function() {
  	var total = 0;
  	var items = Carts.cartWithUsername(Meteor.user().username);

  	//for (var item in items) {
  	for (var i = 0; i < items.length; i++) {
  		var item = items[i];
  		if (item.productPrice) {
  			total += item.number * item.productPrice;
  		}
  	}
  	return total;
  }
});

Template.cart.events({
  'click .btn-order': function(event) {
    if (! Meteor.userId()) {
      return Overlay.open('authOverlay');
    }
    var phone = document.getElementById('phone').value;
    var deliveryAddress = document.getElementById('deliveryAddress').value;
    var deliveryTime = document.getElementById('deliveryTime').value;
    var discountCode = document.getElementById('discountCode').value;
    var description = document.getElementById('description').value;
    if (!phone || phone == '') {
    	alert("请填写联系电话");
    	return false;
    }
    if (!deliveryAddress || deliveryAddress == '') {
    	alert("请填写配送地址");
    	return false;
    }
    if (!deliveryTime || deliveryTime == '') {
    	alert("请填写配送时间");
    	return false;
    }
    if (!discountCode || discountCode == '') {
    	alert("请填写折扣码");
    	return false;
    }
    
    // save order
    var username = Meteor.user().username;
    
  	var items = Carts.cartWithUsername(Meteor.user().username);
  	for (var i = 0; i < items.length; i++) {
  		var item = items[i];
	    Orders.insert({
	      username: username,
	      email: Meteor.user().emails[0].address,
	      productId: item.productId,
	      productName: item.productName,
	      productPrice: item.productPrice,
	      productQuantity: item.number,
	      phone: phone,
	      deliveryAddress: deliveryAddress,
	      deliveryTime: deliveryTime,
	      discountCode: discountCode,
	      description: description,
	      orderDate: new Date(),
	      completed: false,
	      status: 1
	    });
  	}
  	
  	// delete cart
  	Meteor.call("removeCartWithUsername", username);
    
    alert('已下单');
  },
  
  'click .delete': function() {
    if (! Meteor.userId()) {
      return Overlay.open('authOverlay');
    }
    
    var username = Meteor.user().username;
  	// remove the item from cart
  	Meteor.call("removeCart", this._id);
  }
  
});