// 当前用户的订单
Template.orders.helpers({
  orders: function() {
  	return Orders.incompletedOrderWithUsername(Meteor.user().username);
  }
});