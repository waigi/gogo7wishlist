Template.newsSubmit.helpers({
  latestNews: function() {
    return News.latest();
  }
});

Template.newsSubmit.events({
  'submit form': function(event) {
    event.preventDefault();

    var newscontent = $(event.target).find('[name=newscontent]').val();
    News.insert({ text: newscontent, date: new Date });

    alert('');
  }
})