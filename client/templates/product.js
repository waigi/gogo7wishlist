var TAB_KEY = 'productShowTab';

Template.product.created = function() {
  if (Router.current().params.activityId)
    Template.product.setTab('feed');
  else
    Template.product.setTab('product');
}

Template.product.rendered = function () {
  this.$('.product').touchwipe({
    wipeDown: function () {
      if (Session.equals(TAB_KEY, 'product'))
        Template.product.setTab('make')
    },
    preventDefaultEvents: false
  });
  this.$('.attribution-product').touchwipe({
    wipeUp: function () {
      if (! Session.equals(TAB_KEY, 'product'))
        Template.product.setTab('product')
    },
    preventDefaultEvents: false
  });
}

// CSS transitions can't tell the difference between e.g. reaching
//   the "make" tab from the expanded state or the "feed" tab
//   so we need to help the transition out by attaching another
//   class that indicates if the feed tab should slide out of the
//   way smoothly, right away, or after the transition is over
Template.product.setTab = function(tab) {
  var lastTab = Session.get(TAB_KEY);
  Session.set(TAB_KEY, tab);
  
  var fromProduct = (lastTab === 'product') && (tab !== 'product');
  $('.feed-scrollable').toggleClass('instant', fromProduct);

  var toProduct = (lastTab !== 'product') && (tab === 'product');
  $('.feed-scrollable').toggleClass('delayed', toProduct);
}

Template.product.helpers({
  isActiveTab: function(name) {
    return Session.equals(TAB_KEY, name);
  },
  activeTabClass: function() {
    return Session.get(TAB_KEY);
  },
  bookmarked: function() {
    return Meteor.user() && _.include(Meteor.user().bookmarkedProductNames, this.name);
  },
  activities: function() {
    return Activities.find({productName: this.name}, {sort: {date: -1}});
  },
  itemNumber: function() {
  	var itemNumber = 0;
  	var cartItem;
  	if (Meteor.userId()) {
    	cartItem = Carts.cartWithUsernameAndProductId(Meteor.user().username, this._id);
    }
    if (cartItem) {
    	itemNumber = cartItem.number;
    } else {
    	itemNumber = 0;
    }
  	return itemNumber;
  },
  hasItem: function() {
  	var itemNumber = 0;
  	var cartItem;
  	if (Meteor.userId()) {
    	cartItem = Carts.cartWithUsernameAndProductId(Meteor.user().username, this._id);
    }
    if (cartItem) {
    	itemNumber = cartItem.number;
    } else {
    	itemNumber = 0;
    }
  	return itemNumber > 0;
  }
});

Template.product.events({
  'click .js-add-bookmark': function(event) {
    event.preventDefault();

    if (! Meteor.userId())
      return Overlay.open('authOverlay');
    
    Meteor.call('bookmarkProduct', this.name);
  },

  'click .js-remove-bookmark': function(event) {
    event.preventDefault();

    Meteor.call('unbookmarkProduct', this.name);
  },
  
  'click .js-show-product': function(event) {
    event.stopPropagation();
    Template.product.setTab('make')
  },
  
  'click .js-show-feed': function(event) {
    event.stopPropagation();
    Template.product.setTab('feed')
  },
  
  'click .js-uncollapse': function() {
    Template.product.setTab('product')
  },

  'click .js-share': function() {
    Overlay.open('shareOverlay', this);
  },

  'click .image1': function() {
    var url = '/img/products/640x480/'+this.imgName;
    var image = document.getElementById("image-product");
    image.style.backgroundImage = "url('"+url+"')";
  },

  'click .image2': function() {
    var url = '/img/products/640x480/'+this.imgName2;
    var image = document.getElementById("image-product");
    image.style.backgroundImage = "url('"+url+"')";
  },

  'click .image3': function() {
    var url = '/img/products/640x480/'+this.imgName3;
    var image = document.getElementById("image-product");
    image.style.backgroundImage = "url('"+url+"')";
  },

  'click .btn-buy': function() {
    alert('已更新购物车');
  },

  'click .btn-increase': function() {
    if (! Meteor.userId()) {
      return Overlay.open('authOverlay');
    }
    var username = Meteor.user().username;
    //alert(username);
    
    var cartItem = Carts.cartWithUsernameAndProductId(username, this._id);
    
    if (!cartItem) {
	    Carts.insert({ 
	      username: username,
	      productId: this._id,
	      productName: this.name,
	      productPrice: this.ourPrice,
	      number: 1,
	      updatedAt: new Date()
	    });
	    
    } else {
    	
			var itemProperties = {
		    number: cartItem.number + 1,
	      updatedAt: new Date()
	    }
	    
	    Carts.update(cartItem._id, {$set: itemProperties}, function(error) {
	      if (error) {
	        alert(error.reason);
	      } else {
	      }
	    });
    
    }
  },

  'click .btn-decrease': function() {
    if (! Meteor.userId()) {
      return Overlay.open('authOverlay');
    }
    var username = Meteor.user().username;
    //alert(username);
    
    var cartItem = Carts.cartWithUsernameAndProductId(username, this._id);
    
    if (cartItem && cartItem.number > 0) {
    	
			var itemProperties = {
		    number: cartItem.number - 1,
	      updatedAt: new Date()
	    }
	    
	    Carts.update(cartItem._id, {$set: itemProperties}, function(error) {
	      if (error) {
	        alert(error.reason);
	      } else {
	      }
	    });
	    
    } else {
    	// remove the item from cart
    	Meteor.call("removeCartWithUsernameAndProductId", username, this._id);
    }
  }
  
});