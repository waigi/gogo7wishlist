Template.productItem.helpers({
  path: function () {
    return Router.path('product', this.product);
  },
  
  highlightedClass: function () {
    if (this.size === 'large')
      return 'highlighted';
  },
  
  bookmarkCount: function () {
    var count = BookmarkCounts.findOne({productName: this.name});
    return count && count.count;
  }
});