Template.productSubmit.helpers({
		products: function () {
			return Products.find({}, {sort: {displayOrder: -1, createdAt: -1}});
		}
});

Template.productSubmit.events({
  'submit form': function(event) {
    event.preventDefault();

    var name = $(event.target).find('[name=name]').val();
    var brand = $(event.target).find('[name=brand]').val();
    var model = $(event.target).find('[name=model]').val();
    var brief = $(event.target).find('[name=brief]').val();
    var description = $(event.target).find('[name=description]').val();
    var imgName = $(event.target).find('[name=imgName]').val();
    var imgName2 = $(event.target).find('[name=imgName2]').val();
    var imgName3 = $(event.target).find('[name=imgName3]').val();
    var deliveryRegion = $(event.target).find('[name=deliveryRegion]').val();
    var category = $(event.target).find('[name=category]').val();
    var originalCountry = $(event.target).find('[name=originalCountry]').val();
    var ourPrice = $(event.target).find('[name=ourPrice]').val();
    var hkPrice = $(event.target).find('[name=hkPrice]').val();
    var euPrice = $(event.target).find('[name=euPrice]').val();
    var usPrice = $(event.target).find('[name=usPrice]').val();
    var cnPrice = $(event.target).find('[name=cnPrice]').val();
    var stockCount = $(event.target).find('[name=stockCount]').val();
    var totalSoldCount = $(event.target).find('[name=totalSoldCount]').val();
    var highlighted = $(event.target).find('[name=highlighted]').val();
    var promotionFlag = $(event.target).find('[name=promotionFlag]').val();
    var hotFlag = $(event.target).find('[name=hotFlag]').val();
    var displayOrder = $(event.target).find('[name=displayOrder]').val();
var luxryFlag = $(event.target).find('[name=luxryFlag]').val();
var babyFlag = $(event.target).find('[name=babyFlag]').val();
var healthfoodFlag = $(event.target).find('[name=healthfoodFlag]').val();
var cosmeticsFlag = $(event.target).find('[name=cosmeticsFlag]').val();
    
    Products.insert({ 
      name: name,
      brand: brand,
      model: model,
      brief: brief,
      description: description,
      imgName: imgName,
      imgName2: imgName2,
      imgName3: imgName3,
      deliveryRegion: deliveryRegion,
      category: category,
      originalCountry: originalCountry,
      ourPrice: ourPrice,
      hkPrice: hkPrice,
      euPrice: euPrice,
      usPrice: usPrice,
      cnPrice: cnPrice,
      stockCount: stockCount,
      totalSoldCount: totalSoldCount,
      highlighted: event.target.flags[0].checked,
      promotionFlag: event.target.flags[1].checked,
      hotFlag: event.target.flags[2].checked,
luxryFlag: event.target.categoryflags[0].checked,
babyFlag: event.target.categoryflags[1].checked,
healthfoodFlag: event.target.categoryflags[2].checked,
cosmeticsFlag: event.target.categoryflags[3].checked,
      createdAt: new Date(),
      displayOrder: displayOrder
    });
    
		// clear form
    event.target.name.value = "";
    event.target.brand.value = "";
    event.target.model.value = "";
    event.target.brief.value = "";
    event.target.description.value = "";
    event.target.imgName.value = "";
    event.target.imgName2.value = "";
    event.target.imgName3.value = "";
    event.target.deliveryRegion.value = "";
    event.target.category.value = "";
    event.target.originalCountry.value = "";
    event.target.ourPrice.value = "";
    event.target.hkPrice.value = "";
    event.target.euPrice.value = "";
    event.target.usPrice.value = "";
    event.target.cnPrice.value = "";
    event.target.stockCount.value = "";
    event.target.totalSoldCount.value = "";
    event.target.flags[0].checked = false;
    event.target.flags[1].checked = false;
    event.target.flags[2].checked = false;
    event.target.categoryflags[0].checked = false;
    event.target.categoryflags[1].checked = false;
    event.target.categoryflags[2].checked = false;
    event.target.categoryflags[3].checked = false;
    event.target.displayOrder.value = "";
    
		// prevent default form submit
		return false;
  }
  
});

Template.productEdit.helpers({
		products: function () {
			//return Products.find({}, {sort: [["displayOrder": "desc"], ["createdAt": "desc"]]});
			return Products.find({}, {sort: {displayOrder: -1, createdAt: -1}});
		}
});

Template.productEdit.events({
  'submit form': function(event) {
    event.preventDefault();
		
		var currentPostId = this._id;
		
		var productProperties = {
	    name: $(event.target).find('[name=name]').val(),
	    brand: $(event.target).find('[name=brand]').val(),
	    model: $(event.target).find('[name=model]').val(),
	    brief: $(event.target).find('[name=brief]').val(),
	    description: $(event.target).find('[name=description]').val(),
	    imgName: $(event.target).find('[name=imgName]').val(),
	    imgName2: $(event.target).find('[name=imgName2]').val(),
	    imgName3: $(event.target).find('[name=imgName3]').val(),
	    deliveryRegion: $(event.target).find('[name=deliveryRegion]').val(),
	    category: $(event.target).find('[name=category]').val(),
    	originalCountry: $(event.target).find('[name=originalCountry]').val(),
	    ourPrice: $(event.target).find('[name=ourPrice]').val(),
	    hkPrice: $(event.target).find('[name=hkPrice]').val(),
	    euPrice: $(event.target).find('[name=euPrice]').val(),
	    usPrice: $(event.target).find('[name=usPrice]').val(),
	    cnPrice: $(event.target).find('[name=cnPrice]').val(),
	    stockCount: $(event.target).find('[name=stockCount]').val(),
	    totalSoldCount: $(event.target).find('[name=totalSoldCount]').val(),
	    highlighted: event.target.flags[0].checked,
	    promotionFlag: event.target.flags[1].checked,
	    hotFlag: event.target.flags[2].checked,
            luxryFlag: event.target.categoryflags[0].checked,
            babyFlag: event.target.categoryflags[1].checked,
            healthfoodFlag: event.target.categoryflags[2].checked,
            cosmeticsFlag: event.target.categoryflags[3].checked,
	    displayOrder: $(event.target).find('[name=displayOrder]').val()
    }
    
    Products.update(currentPostId, {$set: productProperties}, function(error) {
      if (error) {
        alert(error.reason);
      } else {
      }
    });
    
		// prevent default form submit
		return false;
  }
  
});

  
Products.allow({
	insert: function (userId, doc) {
  	return true;
	}
})


Template.productAdmin.events({
	"click .delete": function() {
		Meteor.call("deleteProduct", this._id);
	}
});

Template.news.helpers({
  latestNews: function() {
    return News.latest();
  }
});

Template.news.events({
  'submit form': function(event) {
    event.preventDefault();

    var newscontent = $(event.target).find('[name=newscontent]').val();
    News.insert({ text: newscontent, date: new Date });

  }
});

Template.orderAdmin.helpers({
  orders: function() {
    return Orders.find({});
  }
});
