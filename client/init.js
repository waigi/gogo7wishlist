Meteor.startup(function() {
  // Potentially prompts the user to enable location services. We do this early
  // on in order to have the most accurate location by the time the user shares
  Geolocation.currentLocation();
  
  /*
  if(!Session.get('shoppingCart')){ //set default if session shopping cart not exist
    var cart = new ShoppingCart();
    Session.setDefault('shoppingCart', cart);
  }
  */
  
  Accounts.ui.config({
	  passwordSignupFields: 'USERNAME_AND_OPTIONAL_EMAIL'
	});

});

/*
_.extend(ShoppingCart, {
  fromJSON: function(json) {
    var obj = new ShoppingCart();
    obj.grandTotal = json.grandTotal;
    obj.Items = json.Items;
    return obj;
  },
});

_.extend(ShoppingCart.prototype, {
  toJSON: function() {
    return {
      grandTotal: this.grandTotal,
      Items: this.Items,
    };
  },
});
*/